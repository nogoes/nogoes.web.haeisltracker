new Vue({
	el: '#v',
	data: {
		navs: [
			{ label: 'today', section: 'today', active: true },
			{ label: 'overall', section: 'overall', active: false }
		],
		poos: 0,
		pees: 0,
		fails: 0,
		stats: []
	},
	mounted() {
		const data = localStorage.getItem('stats');
		if (!data) return;

		this.stats = JSON.parse(data) || [];
		const today = this.stats.find(x => x.date === this.getDate());
		if (!today) return;

		this.poos = today.poos;
		this.pees = today.pees;
		this.fails = today.fails;
	},
	computed: {
		page() {
			return this.navs.find(x => x.active);
		},
		count() {
			switch (this.page.section) {
				case 'today':
					return (+this.poos) + (+this.pees) + (+this.fails);

				case 'overall':
					return this.stats.reduce((acc, x) => {
						acc += (x.poos + x.pees + x.fails);
						return acc;
					}, 0);
			}
		}
	},
	methods: {
		navigateTo(nav) {
			this.navs.find(x => x.active).active = false;
			nav.active = true;
		},
		store() {
			localStorage.setItem('stats', JSON.stringify(this.stats));
		},
		decr(typ) {
			this.calc(typ, -1);
		},
		incr(typ) {
			this.calc(typ, 1);
		},
		calc(typ, adder) {
			console.log(typ);
			switch (typ) {
				case 'poos':
					this.poos += adder;
					break;
				case 'pees':
					this.pees += adder;
					break;
				case 'fails':
					this.fails += adder;
					break;
			}
			this.setStats();
			this.store();
		},
		setStats() {
			const today = this.stats.find(x => x.date === this.getDate());

			if (today) {
				today.poos = this.poos;
				today.pees = this.pees;
				today.fails = this.fails;
				return;
			}

			this.stats.push({
				date: this.getDate(),
				poos: this.poos,
				pees: this.pees,
				fails: this.fails
			})
		},
		getDate(date = new Date()) {
			return date.toLocaleDateString();
		},
		getOverall(typ) {
			return this.stats.reduce((acc, x) => acc += x[typ], 0);
		}
	}
});
